package com.fellows.exc01;

import java.util.function.Consumer;

import org.junit.jupiter.api.Test;

// Callbacks

class Teste05 {

	static void hello(String name, String lastName, Consumer<String> callback) {
		System.out.print(name + " ");
		if(lastName != null) {
			System.out.println(lastName);
		} else {
			callback.accept(name);
		}
	}
	
	@Test
	void callbacks() {
		hello("Fernanda", "Fellows", value -> System.out.println("No surname detected for " + value));
		hello("Fernanda", null, value -> System.out.println("No surname detected for " + value));
	}
}
