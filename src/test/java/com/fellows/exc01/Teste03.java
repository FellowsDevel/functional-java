package com.fellows.exc01;

import static org.junit.Assert.*;
import static org.junit.jupiter.api.Assertions.*;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

class Teste03 {

	class Person {
		private String name;
		private Gender gender;

		public Person(String name, Gender gender) {
			super();
			this.name = name;
			this.gender = gender;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Gender getGender() {
			return gender;
		}

		public void setGender(Gender gender) {
			this.gender = gender;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", gender=" + gender + "]";
		}

	}

	enum Gender {
		MALE, FEMALE
	}

	@Test
	void stream_01() {
		
		List<Person> people = List.of(
			new Person("André", Gender.MALE),
			new Person("Fernanda", Gender.FEMALE),
			new Person("Vitor", Gender.MALE),
			new Person("Ana", Gender.FEMALE)
		);
		
		Set<Gender> genders = people.stream()
				.map(person -> person.gender)
				.collect(Collectors.toSet());
		
		genders.forEach(System.out::println);		
		
		assertTrue(true);
	}

	
	@Test
	void optional() {
		Optional.ofNullable(new Person("asd", Gender.FEMALE))
				.ifPresentOrElse(boj -> System.out.println(boj.getClass()), () -> System.out.println("null"));
	}
}
