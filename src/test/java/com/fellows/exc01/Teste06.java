package com.fellows.exc01;

import java.util.Optional;

import org.junit.jupiter.api.Test;

// Callbacks

class Teste06 {
	@Test
	void optional() {
		
		Optional<String> name = Optional.ofNullable("André");
		System.out.println("name is set? " + name.isPresent());
		System.out.println("name is " + name.orElseGet(() -> "[none]"));
		System.out.println(name.map(s -> "Hey "  + s + " !! ").orElse("hey Stranger!"));
		
	}
}
