package com.fellows.exc01;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.ArrayList;
import java.util.List;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;

class Teste02 {

	class Person {
		private String name;
		private Gender gender;

		public Person(String name, Gender gender) {
			super();
			this.name = name;
			this.gender = gender;
		}

		public String getName() {
			return name;
		}

		public void setName(String name) {
			this.name = name;
		}

		public Gender getGender() {
			return gender;
		}

		public void setGender(Gender gender) {
			this.gender = gender;
		}

		@Override
		public String toString() {
			return "Person [name=" + name + ", gender=" + gender + "]";
		}

	}

	enum Gender {
		MALE, FEMALE
	}

	@Test
	void imperativeProgramming() {
		List<Person> persons = List.of(
			new Person("André", Gender.MALE),
			new Person("Fernanda", Gender.FEMALE),
			new Person("Vitor", Gender.MALE),
			new Person("Ana", Gender.FEMALE)
		);
		
		List<Person> females = new ArrayList<Person>();
		for(Person female : persons) {
			if(Gender.FEMALE.equals(female.gender)) {
				females.add(female);
			}
		}
		for(Person female : females) {
			System.out.println(female);
		}
		
		assertTrue(true);
	}
	
	@Test
	void declarativeProgramming() {
		List<Person> persons = List.of(
			new Person("André", Gender.MALE),
			new Person("Fernanda", Gender.FEMALE),
			new Person("Vitor", Gender.MALE),
			new Person("Ana", Gender.FEMALE)
		);
		
		Predicate<Person> predicate = person -> Gender.MALE.equals(person.gender);
		
		persons.stream()
				.filter(predicate)
				.collect(Collectors.toList())
				.forEach(System.out::println);
		
		assertTrue(true);
	}
	
	@Test
	void functionalNotation() {
		
		Function<Integer, Integer> incrementFunction = number -> number + 1;
		
		int f1 = increment(1);
		int f2 = incrementFunction.apply(1);
		
		assertEquals(f1, f2);
		
		Function<Integer, Integer> multiplyBy10 = number -> number * 10;
		Function<Integer, Integer> sumAndMultiply = incrementFunction.andThen(multiplyBy10);
		
		assertEquals(20, sumAndMultiply.apply(1));
		
	}
	
	@Test
	void chainingFunctional() {
		
		Function<Integer, Integer> incrementFunction = number -> number + 1;
		Function<Integer, Integer> multiplyBy10 = number -> number * 10;
		Function<Integer, Integer> sumAndMultiply = incrementFunction.andThen(multiplyBy10);
		
		assertEquals(20, sumAndMultiply.apply(1));
		
	}
	
	private int increment(int number) {
		return number + 1;
	}
	
	@Test
	void biFunction() {
		BiFunction<Integer, Integer, Integer> incrementAndMultiply = (incOne, multiply) -> (incOne + 1) * multiply;
		
		System.out.println(incrementAndMultiply.apply(4, 10));
	}
	
	private class Customer{
		private final String name;
		private final String phone;
		
		public Customer(String name, String phone) {
			super();
			this.name = name;
			this.phone = phone;
		}
	}
	
	@Test
	void consumer() {
		Customer c1 = new Customer("André", "1123123");
		
		Consumer<Customer> greetCustomer = customer -> System.out.println("Hello " + customer.name + ", phone: " + customer.phone);
		
		greetCustomer.accept(c1);
	}

	@Test
	void predicate() {
		Predicate<String> lenGt5 = cust -> cust.length() > 5;
		
		assertTrue(lenGt5.test("Fellows"));
	}
	
	//
	@Test
	void supplier() {
		Supplier<String> s = () -> "teste";
		assertTrue("teste".equals(s.get()));
	}


}
