package com.fellows.exc01;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.time.Period;
import java.util.function.Function;

import org.junit.jupiter.api.Test;

class Teste04 {

	class Customer {
		private final String name;
		private final String email;
		private final String phone;
		private final LocalDate birth;

		public Customer(String name, String email, String phone, LocalDate birth) {
			super();
			this.name = name;
			this.email = email;
			this.phone = phone;
			this.birth = birth;
		}

		public String getName() {
			return name;
		}

		public String getEmail() {
			return email;
		}

		public String getPhone() {
			return phone;
		}

		public LocalDate getBirth() {
			return birth;
		}

	}

	class CustomerValidatorService {
		private boolean isEmailValid(String email) {
			return email != null && email.contains("@");
		}

		private boolean isPhoneValid(String phone) {
			return phone != null && phone.startsWith("+");
		}

		private boolean isAdult(LocalDate dob) {
			return dob != null && Period.between(dob, LocalDate.now()).getYears() > 18;
		}

		public boolean isValid(Customer customer) {
			return isEmailValid(customer.getEmail()) && isPhoneValid(customer.getPhone())
					&& isAdult(customer.getBirth());
		}
	}

	@Test
	void normal() {

		Customer c1 = new Customer("Andre", "andre@fellows.com", "+55 81 12341234", LocalDate.of(1975, 03, 17));

		CustomerValidatorService val = new CustomerValidatorService();

		assertTrue(val.isValid(c1));
	}

	
	
	
	// Combinator Pattern
	
	enum ValidationResult {
		SUCCESS, EMAIL_NOT_VALID, PHONE_NOT_VALID, NOT_AN_ADULT
	}

	interface CustomerRegistrationValidator extends Function<Customer, ValidationResult> {

		static CustomerRegistrationValidator isEmailValid() {
			return customer -> customer.getEmail() != null && customer.getEmail().contains("@")
					? ValidationResult.SUCCESS
					: ValidationResult.EMAIL_NOT_VALID;
		}

		static CustomerRegistrationValidator isPhoneValid() {
			return customer -> customer.getPhone() != null && customer.getPhone().startsWith("+")
					? ValidationResult.SUCCESS
					: ValidationResult.PHONE_NOT_VALID;
		}

		static CustomerRegistrationValidator isAdult() {
			return customer -> customer.getBirth() != null
					&& Period.between(customer.getBirth(), LocalDate.now()).getYears() > 18 
						? ValidationResult.SUCCESS
						: ValidationResult.NOT_AN_ADULT;
		}
		
		default CustomerRegistrationValidator and (CustomerRegistrationValidator other) {
			return customer -> {
				ValidationResult result = this.apply(customer);
				return result.equals(ValidationResult.SUCCESS) ? other.apply(customer) : result;
			};
		}
	}
	
	@Test
	void combinator() {
		Customer c1 = new Customer("Andre", "andre@fellows.com", "+55 81 12341234", LocalDate.of(1975, 03, 17));
		
		ValidationResult result = CustomerRegistrationValidator
									.isEmailValid()
									.and(CustomerRegistrationValidator.isPhoneValid())
									.and(CustomerRegistrationValidator.isAdult())
									.apply(c1);
		
		assertTrue(ValidationResult.SUCCESS.equals(result));
	}
}
