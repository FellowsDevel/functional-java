package com.fellows.exc01;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

/**
 * A interface deve conter apenas um método para permitir
 * a notação de Lambda para implementá-la sem a 
 * necessidade de criação de classes
 * 
 * @author andre
 *
 */
@DisplayName("Implementando um interface com lambda")
class Teste01 {

	public Integer calcular(Integer a, Integer b, Matematica m) {
		System.out.println(m.operar(a, b));
		return m.operar(a, b);
	}

	@Test
	void test() {
		assertEquals(30, calcular(10, 20, (a, b) -> a + b));
		assertEquals(200, calcular(10, 20, (a, b) -> a * b));
		assertEquals(1000, calcular(250, 4, (a, b) -> a * b));
	}
	
	@Test
	void test2() {
		Runnable r1 = () -> System.out.println("Executando em thread");
		Runnable r2 = () -> {
			for (int i = 0; i < 10; i++) {
				System.out.println(i);
			}
		};
		
		Thread t1 = new Thread(r1);
		Thread t2 = new Thread(r2);
		
		t1.start();
		t2.start();
		
		new Thread(()-> System.out.println("direto da thread")).start();
		
	}

}
