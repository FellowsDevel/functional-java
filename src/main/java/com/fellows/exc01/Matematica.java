package com.fellows.exc01;

/**
 * A interface deve conter apenas um método para permitir
 * a notação de Lambda para implementá-la sem a 
 * necessidade de criação de classes
 * 
 * @author andre
 *
 */
public interface Matematica {
	Integer operar(Integer a, Integer b);
}
